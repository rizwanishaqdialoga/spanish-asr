from __future__ import print_function
import argparse
import time
import numpy as np
import soundfile as sf
from glob import glob
import os
import json
import codecs
from scipy.signal import spectrogram, lfilter, resample_poly


from grpc.beta import implementations
from tensorflow.contrib.util import make_tensor_proto

from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2
import tensorflow as tf
from dictionary import dictiony_char_int
os.environ["CUDA_VISIBLE_DEVICES"]="-1"   



char2int,idx2char = dictiony_char_int()


#char2int,idx2char = dictiony_char_int()

def rm_dc_n_dither(sin,fs):
    alpha = 0.999
    sin = lfilter([1,-1],[1,-alpha],sin) # remove DC
    dither = np.random.rand(np.size(sin))+np.random.rand(np.size(sin))-1
    s_pow = np.std(sin)
    sout = sin + 1e-6*s_pow*dither
    return sout


def map_function(file):
    s, fs = sf.read(file)
    if s.ndim>1:
        s = s[:,0]
    #if s.dtype=='int16':
        #s = s/32768.0
    #else:
        #s = s/np.max(np.abs(s))
    s = s/np.max(np.abs(s))
    if fs!=8000:
        s = resample_poly(s,8000,fs)
        s = s/max(abs(s))
    s = rm_dc_n_dither(s,fs)

    return s.astype('float32')



def decoder_tf_session(logits):
    corpus=codecs.open('./LM/vocab.txt', 'r', 'utf8').read()
    chars = " abcdefghijklmnopqrstuvwxyzáéíñóúü"
    wordChars = "abcdefghijklmnopqrstuvwxyzáéíñóúü"
    word_beam_search_module = tf.load_op_library('./CTCWordBeamSearch/cpp/proj/TFWordBeamSearch.so')
    lm_decoded = word_beam_search_module.word_beam_search(tf.nn.softmax(logits),
                                                            125, 'Words', 0.0,
                                                            corpus.encode('utf8'),
                                                            chars.encode('utf8'),
                                                            wordChars.encode('utf8'))
    lm_decoded_nGrams = word_beam_search_module.word_beam_search(tf.nn.softmax(logits),
                                                                    125, 'NGrams', 0.0,
                                                                    corpus.encode('utf8'),
                                                                    chars.encode('utf8'),
                                                                    wordChars.encode('utf8'))
    return lm_decoded, lm_decoded_nGrams

def decoder_input():
    logit = tf.placeholder(tf.float32, [None,None,35])
    return logit




def run(host, port, model, signature_name):
    logit = decoder_input()
    lm_decoded, lm_decoded_nGrams = decoder_tf_session(logit)
    sess = tf.Session()

    # channel = grpc.insecure_channel('%s:%d' % (host, port))
    channel = implementations.insecure_channel(host, port)
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)
    request = predict_pb2.PredictRequest()
    request.model_spec.name = model
    request.model_spec.signature_name = signature_name
  

    # read audio files
    wav_files = glob(os.path.join('/tertiary/rizwan/spanish','*.wav'))
    for file in wav_files:
        data = map_function(file)
        #with open(file.replace('.wav','.txt'),'r') as f:
            #text = f.readlines()[0]


        # Call classification model to make prediction on the image
        request.inputs['audio'].CopyFrom(make_tensor_proto(data, shape=[1,len(data)], dtype=tf.float32))
        response = stub.Predict(request, 10.0)  
        shape = response.outputs['shape'].int_val
        #print(response)
        
        logits = np.reshape(response.outputs['logit'].float_val,shape)
        d1,d2 = sess.run([lm_decoded, lm_decoded_nGrams], feed_dict={logit:logits})
        pred = response.outputs['preds'].int_val
        s2 = ''.join([idx2char.get(idx,'') for idx in pred])
        s = ''.join([idx2char.get(idx,'') for idx in d1[0]])
        s1 = ''.join([idx2char.get(idx,'') for idx in d2[0]])
        print(file)
        print(s)
        print(s1)
        print(s2)
        #print('original:', text)
        print('\n')
        

        




        # Reference:
        # How to access nested values
        # https://stackoverflow.com/questions/44785847/how-to-retrieve-float-val-from-a-predictresponse-object
        #print(result)




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='Tensorflow server host name', default='100.100.100.44', type=str)
    parser.add_argument('--port', help='Tensorflow server port number', default=9000, type=int)
    parser.add_argument('--model', help='model name', default='speech',type=str)
    parser.add_argument('--signature_name', help='Signature name of saved TF model',
                        default='serving_default', type=str)

    args = parser.parse_args()
    run(args.host, args.port, args.model, args.signature_name)
