# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import tensorflow as tf
from utils import CBHG
from dictionary import dictiony_char_int
#from tensorflow.python import pywrap_tensorflow
tf.logging.set_verbosity(tf.logging.INFO)


###
#import tensorflow as tf
#from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
#from tensorflow.python import pywrap_tensorflow
###


def simple_lstm(units,rate):
    layer = tf.nn.rnn_cell.LSTMCell(num_units=units, name='basic_lstm_cell')
    return tf.nn.rnn_cell.DropoutWrapper(layer,rate)




def residual_cell(units, rate, num_layers):
    stacked_lstm = [simple_lstm(units, rate)]
    for _ in range(num_layers-1):
        stacked_lstm.append(tf.nn.rnn_cell.ResidualWrapper(simple_lstm(units, rate)))
    return stacked_lstm




def attend(inputs, sequence_length, attention_size=300, attention_depth=1):
    
    inputs_shape = inputs.shape
    #sequence_length = inputs_shape[1].value
    final_layer_size = inputs_shape[2].value

    x = tf.transpose(inputs,[1,0,2])
    x = tf.reshape(x, [-1, final_layer_size])
    for _ in range(attention_depth-1):
        x = tf.layers.dense(x, attention_size, activation = tf.nn.relu)
    x = tf.layers.dense(x, 1, activation = None)
    logits = tf.reshape(x, [-1,sequence_length, 1])
    alphas = tf.nn.softmax(logits, axis = 1)
    alphas = tf.transpose(alphas, [1,0,2])

    output = inputs * alphas

    return output



def network(features, mode, params):

    # Default parameters
    num_features = params['num_features']
    rnn_size = params['rnn_size']
    keep_prob = params['keep_prob']
    num_layers = params['num_layers']
    attenion_size = params['attenion_size']
    num_classes = params['num_classes']
    is_training = mode==tf.estimator.ModeKeys.TRAIN

    ## Network
    #seq_lens = tf.count_nonzero(tf.reduce_sum(tf.abs(features), -1), 1, dtype=tf.int32)
    seq_lens = features['seq_lens']

    enc_input = CBHG(features['spectrogram'], num_features, is_training=is_training)
    shape = tf.shape(enc_input)
    batch_s, max_timesteps = shape[0],shape[1]
    batch_x = tf.transpose(enc_input,[1,0,2])

    
    cell_bw = tf.nn.rnn_cell.MultiRNNCell(residual_cell(rnn_size,rate=keep_prob if is_training==True else 1., num_layers=num_layers))
    cell_fw = tf.nn.rnn_cell.MultiRNNCell(residual_cell(rnn_size,rate=keep_prob if is_training==True else 1., num_layers=num_layers))


    outputs, states = tf.nn.bidirectional_dynamic_rnn(cell_fw=cell_fw,
                                                      cell_bw=cell_bw,
                                                      inputs = batch_x,
                                                      sequence_length=seq_lens,
                                                      time_major=True,
                                                      dtype=tf.float32)





    outputs = tf.concat(outputs,2)
    #outputs = attend(outputs, tf.reduce_max(seq_lens),attenion_size)
    outputs = tf.reshape(outputs, [-1, 2*rnn_size])
    
    
    #reg = tf.contrib.layers.l2_regularizer(0.00001)
    layer_3= tf.layers.dense(inputs=outputs, 
                            units=4*rnn_size, 
                            activation=tf.nn.relu, 
                            kernel_regularizer=tf.contrib.layers.l2_regularizer(0.0001))
    layer_3 = tf.nn.dropout(layer_3, keep_prob=keep_prob if is_training==True else 1.)


    # Final_fully_connected_layer
    logits= tf.layers.dense(inputs=layer_3, units=num_classes, activation=None)
    logits = tf.reshape(logits, [-1, batch_s, num_classes])
    


    return logits



def optimizer_fn(cost, initial_learning_rate):
    ## Optimizer
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    opt = tf.train.AdamOptimizer(initial_learning_rate)
    #Next 3 lines used to clip gradients {Prevent gradient explosion problem}
    #gradients = tf.gradients(cost, tf.trainable_variables())
    #gradients = tf.gradients(cost, train_able_variable)
    #clipped_grads, _ = tf.clip_by_global_norm(gradients, 1.0)
    #train_optimizer = opt.apply_gradients(zip(clipped_grads, tf.trainable_variables()), global_step=tf.train.get_global_step())
    #train_optimizer = opt.apply_gradients(zip(clipped_grads, train_able_variable), global_step=tf.train.get_global_step())
    with tf.control_dependencies(update_ops):
        train_optimizer = opt.minimize(loss=cost, global_step=tf.train.get_global_step())

    return train_optimizer

def loss_fn(logits, labels, seq_lens):
    loss = tf.nn.ctc_loss(labels, logits, seq_lens, ignore_longer_outputs_than_inputs=True)
    ctc_cost = tf.reduce_mean(loss)
    tf.losses.add_loss(ctc_cost)
    cost = tf.losses.get_total_loss()

    return cost

def config_fn():
    per_process_gpu_memory_fraction = 1.0 / 1.0
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=per_process_gpu_memory_fraction,
                                allow_growth=True)
    tf_config = tf.ConfigProto(gpu_options=gpu_options,
                               allow_soft_placement=True)
    config = tf.estimator.RunConfig(save_summary_steps=1000,
                                    save_checkpoints_steps=1000,
                                    session_config=tf_config,
                                    keep_checkpoint_every_n_hours=1,
                                    log_step_count_steps=100,
                                    keep_checkpoint_max=50)
    return config

def model_fn(features, labels, mode, params):
    logits = network(features, mode, params)
    #list_of_variable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    # Network
    decoded, log_prob = tf.nn.ctc_greedy_decoder(logits, sequence_length=features['seq_lens'])
    #decoded, log_prob = tf.nn.ctc_beam_search_decoder(logits, sequence_length=features['seq_lens'])
    # decoding
    decoded = tf.to_int32(decoded[0])
    preds = tf.sparse_tensor_to_dense(decoded, default_value=-1)
    predictions = {'preds': preds, 'logit':logits, 'shape':tf.shape(logits)}
    # Prediction

    if mode == tf.estimator.ModeKeys.PREDICT:
        export_outputs = {
            tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
            tf.estimator.export.PredictOutput(outputs=predictions)}
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs= export_outputs)
        #return tf.estimator.EstimatorSpec(mode=mode)

    labels = tf.contrib.layers.dense_to_sparse(labels,eos_token=500)
    loss = loss_fn(logits,labels,features['seq_lens'])
    tf.summary.scalar('loss', loss)
    edit_dist_op = tf.reduce_mean(tf.edit_distance(decoded, labels))
    tf.summary.scalar('edit_dist', edit_dist_op)
    lth = tf.train.LoggingTensorHook({'edit_dist': edit_dist_op}, every_n_iter=100)
    if mode == tf.estimator.ModeKeys.TRAIN:
        train_op = optimizer_fn(loss,params['learning_rate'])

        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op, training_hooks=[lth])

    if mode == tf.estimator.ModeKeys.EVAL:
        eval_metric_ops = {'edit_dist': tf.metrics.mean(edit_dist_op)}
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops, evaluation_hooks=[lth])





def serving_input_receiver_fn():
    def _spectrogram(reciever_tensors, frame_length=200, frame_step=120, fft_length=1024, sample_rate=8000, log_offset=1e-12):
        stft = tf.contrib.signal.stft(reciever_tensors['audio'],
                                  frame_length=frame_length,
                                  frame_step=frame_step,
                                  fft_length=fft_length,
                                  window_fn=tf.contrib.signal.hann_window)
        spectrogram = tf.div(tf.log(tf.abs(stft)+log_offset),tf.log(tf.constant(10.,dtype=tf.float32)))
        b_mean,b_variance = tf.nn.moments(spectrogram,axes=[-2])
        spectrogram = tf.nn.batch_normalization(x=spectrogram,mean=b_mean,variance=b_variance, offset=None, scale=None, variance_epsilon=1e-6)
        seq_lens = tf.shape(spectrogram)[-2][None]
        return spectrogram, seq_lens
    """
    This is used to define inputs to serve the model
    """
    receiver_tensors = {
        'audio': tf.placeholder(tf.float32, [None,None])
    }
    spectrogram, seq_lens = _spectrogram(receiver_tensors)
    features = {
        'spectrogram': spectrogram,
        'seq_lens': seq_lens
    }


    return tf.estimator.export.ServingInputReceiver(receiver_tensors=receiver_tensors,
                                                    features=features)




class Speech2Text():
    def __init__(self, rnn_size, num_layers, num_classes, num_features, learning_rate):
        params = {
            'rnn_size': rnn_size,
            'num_layers': num_layers,
            'num_classes': num_classes,
            'num_features':num_features,
            'learning_rate':learning_rate,
            'keep_prob':0.95,
            'attenion_size':300
        }
        config = config_fn()
        #self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='hdfs://hadoop-cluster-dialoga/users/rizwan/speech_2_text/model/',config=config)
        #latest_ckp = tf.train.latest_checkpoint('./warm_from')
        #reader = pywrap_tensorflow.NewCheckpointReader(latest_ckp)
        #var_to_shape_map = reader.get_variable_to_shape_map()
        #print(len(var_to_shape_map))
        #del(var_to_shape_map['fully_connected_1/weights'])
        #del(var_to_shape_map['fully_connected_1/biases/Adam'])
        #print(len(var_to_shape_map))
        #ws = tf.estimator.WarmStartSettings(ckpt_to_initialize_from="./warm_from", 
                                            #vars_to_warm_start='^(bidirectional_rnn.*|conv1d.*|prenet.*|highwaynet.*)')
        #ws = tf.estimator.WarmStartSettings(ckpt_to_initialize_from="./warm_from")
        #self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='./model',config=config, warm_start_from= ws)
        self.model = tf.estimator.Estimator(model_fn=model_fn,params=params,model_dir='./model',config=config)


    def fit(self, train_input_fn):
        self.model.train(input_fn=train_input_fn)

    def evaluate(self, eval_input_fn):
        eval_results = self.model.evaluate(input_fn=eval_input_fn)
        return eval_results

    def train_and_evaluat(self,train_input_fn,eval_input_fn):
        train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn)
        eval_spec = tf.estimator.EvalSpec(input_fn=eval_input_fn, start_delay_secs=3600*3)
        tf.estimator.train_and_evaluate(self.model, train_spec, eval_spec)

    def export_model(self):
        self.model.export_savedmodel('./pb', serving_input_receiver_fn=serving_input_receiver_fn)


    
